package test;

import static org.junit.Assert.*;

import org.junit.Test;

import time.Time;

public class TimeTest {

	
	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		Time.getTotalMilliseconds("12:05:50:0A");
		fail("not implemented");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		Time.getTotalMilliseconds("12:05:50:1000");
		fail("invalid number of milliseconds");
	}
	
	////////////////////////////////////
	
	/* 
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}

	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}
	
	*/
}
